﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public float pointsPerUnitsTravelled = 1.0f;
    public float gameSpeed = 10.0f;

    private bool gameOver = false;
    private float score = 0.0f;
    private bool hasSaved = false;

    private static float highScore = 0.0f;

    // Use this for initialization
    void Start()
    {
        Instance = this;
        LoadHighScore();
    }

    // Update is called once per frame
    void Update()
    {
        // Game Over Condition
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            gameOver = true;
        }

        if (gameOver)
        {
            if (!hasSaved)
            {
                SaveHighScore();
                hasSaved = true;
            }

            if (Input.anyKeyDown)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        // Score calculations
        if (!gameOver)
        {
            score += pointsPerUnitsTravelled * gameSpeed * Time.deltaTime;
            if (score > highScore)
            {
                highScore = score;
            }
        }
    }

    void SaveHighScore()
    {
        PlayerPrefs.SetInt("HighScore", (int)highScore);
        PlayerPrefs.Save();
    }

    void LoadHighScore()
    {
        highScore = PlayerPrefs.GetInt("HighScore");
    }

    void OnGUI()
    {
        GUILayout.Label("Score: " + ((int)score).ToString());
        GUILayout.Label("High Score: " + ((int)highScore).ToString());

        if (gameOver)
        {
            GUILayout.Label("Game Over! Press any key to reset!");
        }
    }
}
